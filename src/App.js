/*import logo from './logo.svg';*/
import React from 'react';
import Content from './components/Content';
import Header from './components/Header';
import InfoPage from './components/Infopage';
import NavF from './components/NavF';
import NavH from './components/NavH';
import Profile from './components/Profile';
import '../src/App.css';


function App() {
  return ( <div className='appWrapper'>
     
     <Header />
     <Profile />
     <NavH />
     <NavF />      
     <Content />
     <InfoPage />
    
 </div>

);
   
}


export default App;
